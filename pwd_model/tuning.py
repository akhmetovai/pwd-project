import os
import joblib
import pandas as pd
import numpy as np
import xgboost as xgb
from hyperopt import fmin, tpe, STATUS_OK, STATUS_FAIL, Trials, hp
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error


class HyperOptTuning:

    def __init__(self, X, y):
        xgb_reg_params = {
            'learning_rate' : hp.loguniform('learning_rate', np.log(0.005), np.log(0.2)),
            #'learning_rate':    hp.choice('learning_rate',    np.arange(0.05, 0.31, 0.05)),
            'max_depth':        hp.choice('max_depth',        np.arange(5, 16, 1, dtype=int)),
            'min_child_weight': hp.choice('min_child_weight', np.arange(1, 8, 1, dtype=int)),
            #'colsample_bytree': hp.choice('colsample_bytree', np.arange(0.3, 0.8, 0.1)),
            'subsample':        hp.uniform('subsample', 0.8, 1),
            'n_estimators':     100,
        }
        xgb_fit_params = {
            'eval_metric': 'rmse',
            'early_stopping_rounds': 10,
            'verbose': False
        }
        self.xgb_para = {
            'reg_params': xgb_reg_params,
            'fit_params': xgb_fit_params,
            'loss_func': lambda y, pred: np.sqrt(mean_squared_error(y, pred)),
        }
        self.X, self.y = X, y
        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(X, y, train_size=0.8, random_state=0)


    def _process(self, fn_name, space, trials, algo, max_evals):
        fn = getattr(self, fn_name)
        try:
            result = fmin(fn=fn, space=space, algo=algo, max_evals=max_evals, trials=trials)
        except Exception as e:
            return {'status': STATUS_FAIL, 'exception': str(e)}
        return result, trials
        

    def xgb_reg(self, para):
        reg = xgb.XGBRegressor(**para['reg_params'])
        return self.train_reg(reg, para)
    

    def train_reg(self, reg, para):
        reg.fit(self.X_train, self.y_train,
                eval_set=[(self.X_train, self.y_train), (self.X_test, self.y_test)],
                **para['fit_params'])
        pred = reg.predict(self.X_test)
        loss = para['loss_func'](self.y_test, pred)
        return {'loss': loss, 'status': STATUS_OK}
    
    
    def build_model(self):
        xgb_opt = self._process(fn_name='xgb_reg', space=self.xgb_para, trials=Trials(), algo=tpe.suggest, max_evals=10)
        return xgb.XGBRegressor(**xgb_opt)

